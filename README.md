**Help Without Judgment README.md**

*Project Goal*
To make short-term volunteering request and fulfillment easier.

*Description*
A full-stack web application using Django.
Login/registration
User dashboard that enables users to create and/or accept service requests, 
schedule meetups, leave feedback, view past requests, view user stats, and interact with the Charity 
Navigator API to find local non-profits by zip-code for user needs outside of project scope. 
User-to-user instant messaging feature, utilizing Django Channels & Redis.